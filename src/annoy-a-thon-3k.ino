#include <SPI.h>
#include "LCD_functions.h"
#include "SoftwareSerial.h"
#include "DFRobotDFPlayerMini.h"
#include "Arduino.h"

SoftwareSerial software_serial(12, 10); // RX, TX, for connecting with the DFPlayer
DFRobotDFPlayerMini myDFPlayer; // DFPlayer Mini MP3 player

// --------------------------------------------------------------
//  Soundboard
// --------------------------------------------------------------
#define BUSY_PIN 8
int current_track = 0; // Current track to play
int playing_track=0; // Currently playing track
bool loop_playback = false; // Loop playback of tracks
bool update_display = true;

#define NUMBER_OF_TRACKS 25
char* sounds[NUMBER_OF_TRACKS] = {
  "Airhorn",
  "Badum tss",
  "Yaaaay",
  "Trololo",
  "Sad trombone", // 5
  "Drama",
  "Applause",
  "Elevator",
  "Happy",
  "Crickets", // 10
  "Screaming",
  "Halleluja",
  "Lalalala",
  "Saxophone",
  "Fart", // 15
  "Evil laugh",
  "Wrong",
  "PPAP",
  "Notification",
  "The feels", // 20
  "Jeopardy",
  "Tetris",
  "Titanic",
  "Aaaw",
  "1 UP" //25
};

// --------------------------------------------------------------
//  Rotary encoder 
// --------------------------------------------------------------
#define UP 1 // Rotary encoder moves UP/RIGHT.
#define DOWN -1 // Rotary encoder moves DOWN/LEFT.
#define STATIC 0 // Rotary encoder hasn't moved.

#define encoder0PinA 2
#define encoder0PinB 3
#define encoder0Btn 4

int rotary_direction = STATIC;
int encoder0Pos = 0; // Current encoder position.
int valRotary, lastValRotary;

bool is_button_pressed = false;
bool is_button_released = false;
long last_press = 0;

// --------------------------------------------------------------
//  Device modes
// --------------------------------------------------------------
#define MODE_SELECTION -1 // Select a mode.
#define MODE_SOUNDBOARD 0 // Select a track to play.
#define MODE_VOLUME 1 // Select volume.
#define MODE_EQ_SELECT 2 // Select equalizer preset
#define MODE_LOOP 3 // Turn looping playback on/off
#define MODE_RANDOM 4 // Play random sounds at random intervals

#define NUMBER_OF_MODES 5 // Number of available modes.

char* modes[NUMBER_OF_MODES] = {"Soundboard", "Volume", "Equalizer", "Loop", "Random"};
int current_mode = MODE_SOUNDBOARD; // Current (default/starting) operation mode.
int selected_mode = current_mode; // Needed as temp storage when selecting a new mode.
bool mode_changed = false;

// --------------------------------------------------------------
//  Random mode
// --------------------------------------------------------------
long next_sound = 0; // When the next sound will be played

// --------------------------------------------------------------
//  Equalizer presets
// --------------------------------------------------------------
#define NUMBER_OF_EQ_PRESETS 6 // Number of available equalizer presets.

char* eq_presets[NUMBER_OF_EQ_PRESETS] = {"Normal", "Pop", "Rock", "Jazz", "Classic", "Bass" };

int current_eq_preset = DFPLAYER_EQ_NORMAL; // Current (default/starting) equalizer preset.
int selected_eq_preset = current_eq_preset;

// --------------------------------------------------------------
//  Volume
// --------------------------------------------------------------
#define MAX_VOLUME 30 // Maximum volume.

int current_volume = 10; // Current (default/starting) DFplayer volume.


void setup()
{
  pinMode(BUSY_PIN, INPUT_PULLUP);
  
  pinMode(encoder0PinA, INPUT_PULLUP);
  pinMode(encoder0PinB, INPUT_PULLUP);
  pinMode(encoder0Btn, INPUT_PULLUP);
  attachInterrupt(0, doEncoder, CHANGE);
  
  software_serial.begin(9600);

#ifdef DEBUG
  Serial.begin(115200);
  Serial.println(F("Initializing DFPlayer"));
#endif
  
  if (!myDFPlayer.begin(software_serial)) {
#ifdef _DEBUG
    Serial.println(F("Unable to begin"));
    Serial.println(F("1.Please recheck the connection!"));
    Serial.println(F("2.Please insert the SD card!"));
#endif
    while(1);
  }

#ifdef _DEBUG
  Serial.println(F("DFPlayer Mini online."));
#endif

  myDFPlayer.setTimeOut(250); //Set serial communictaion time out
  myDFPlayer.volume(current_volume);  //Set volume. From 0 to 30.

  lcdBegin(); // This will setup our pins, and initialize the LCD
  setContrast(40); // Good values range from 40-60
}

void loop() {
  handleEncoder(); 
  handleAction();
}
 
void handleEncoder() {   
  bool button_pressed = digitalRead(encoder0Btn) == 0; // on: 0, off: 1
  rotary_direction = STATIC;
  
  if (valRotary < lastValRotary) {
    rotary_direction = DOWN;
  } else if (valRotary > lastValRotary) {
    rotary_direction = UP;
  }

  if (button_pressed) {
    if (last_press == 0)
      last_press = millis();
    
    delay(10);  // Bounce, bounce, bounce
    is_button_pressed = true;
    is_button_released = false;
  } else {
    // Act when RELEASING the button
    is_button_released = is_button_pressed;
    is_button_pressed = false;
    last_press = 0;
  }

  lastValRotary = valRotary;
}

// -------------------------------------------------------------
//  ACTION HANDLING
// -------------------------------------------------------------

void handleAction() {

  // Is button still BEING pressed? Then check duration.
  if (last_press !=0 && is_button_pressed && !is_button_released) {
    long delta = millis() - last_press;
    
    if(delta >=1500) {
      update_display = true;
      is_button_pressed = false; // Will otherwise instantly select another mode...
      current_mode = MODE_SELECTION;
      handleMode(rotary_direction, false);
    }
  }
  else if (is_button_released || rotary_direction != STATIC || current_mode == MODE_RANDOM) {
    handleMode(rotary_direction, true); 
  }
}

void handleMode(int direction, bool button_pressed) {

    int tmp_current_mode = current_mode;
    
    switch(current_mode) {
      case MODE_VOLUME: 
        handleVolumeMode(direction, button_pressed);
        break;
      case MODE_SOUNDBOARD:
        handleSoundBoardMode(direction, button_pressed);
        break;
      case MODE_SELECTION:
        handleSwitchingMode(direction, button_pressed);
        break;
      case MODE_EQ_SELECT:
        handleEqualizerMode(direction, button_pressed);
        break;
      case MODE_LOOP:
        handleToggleLoop(direction, button_pressed);
        break;
      case MODE_RANDOM:
        handleRandom(direction, button_pressed);
        break;
      default:
        break;
    }

    if(tmp_current_mode!= current_mode) {
      update_display = true;
      // Mode has changed, handle new mode
      is_button_pressed = false; // Will otherwise instantly select another mode...
      handleMode(STATIC, false);
    }
}

// -------------------------------------------------------------
//  HANDLE EQUALIZER SELECTION
// -------------------------------------------------------------

void handleEqualizerMode(int direction, bool button_pressed) {
      
  if(direction == UP) {
    selected_eq_preset += 1;
    if(selected_eq_preset >= NUMBER_OF_EQ_PRESETS)
      selected_eq_preset  = 0;
    update_display = true;
  }
  else if(direction == DOWN) {
    selected_eq_preset -= 1;

    if(selected_eq_preset  < 0)
      selected_eq_preset = NUMBER_OF_EQ_PRESETS-1;
    update_display = true;
  }
  else if(direction == STATIC && button_pressed) {
    current_eq_preset  = selected_eq_preset;
    myDFPlayer.EQ(current_eq_preset);
    current_mode = MODE_SELECTION;
  }

  if (update_display) {
    displayEqPresets();
    update_display = false;
  }
}

void displayEqPresets() {

  clearDisplay(WHITE);
  
  for(int i=0; i< NUMBER_OF_EQ_PRESETS; i++) {
    String title = eq_presets[i];

    if(selected_eq_preset == i) {
      setStr(title.c_str(), 0, (i*8)+1, WHITE);
    } else {
      setStr(title.c_str(), 0, (i*8)+1, BLACK);
    }
  }

  updateDisplay();
}

// -------------------------------------------------------------
//  HANDLE SETTING MODE_LOOP
// -------------------------------------------------------------
void handleToggleLoop(int direction, bool button_pressed) {  
  loop_playback = !loop_playback;

  if(!loop_playback) {
    myDFPlayer.disableLoop();
  }

  current_mode = MODE_SELECTION; // Immediately return to mode selection.
}

// -------------------------------------------------------------
//  HANDLE RANDOM
// -------------------------------------------------------------

void handleRandom(int direction, bool button_pressed) {
 
  if(next_sound == 0) {
    setRandom();
  }

  if (millis() >= next_sound) {
    playTrack();
    setRandom();
  }

  displayRandom();
  delay(250); 
}

void setRandom() {
  long rnd = random(10,60);
  next_sound = (rnd * 1000) + millis();
  current_track = random(0, NUMBER_OF_TRACKS);
}

void displayRandom() {

  clearDisplay(WHITE);
  
  int delta = ((next_sound - millis())/1000);
  setStr("Playing:", 0, 0, BLACK);
  setStr(sounds[current_track], 0, 8, BLACK);
  String title = (String) "In: " + delta + " sec.";
  setStr(title.c_str(), 0, 24, BLACK);
  
  updateDisplay();
}

// -------------------------------------------------------------
//  MODE SELECTION MODE
// -------------------------------------------------------------

void handleSwitchingMode(int direction, bool button_pressed) {
      
  if(direction == UP) {
    selected_mode += 1;
    if(selected_mode >= NUMBER_OF_MODES)
      selected_mode = 0;
    update_display = true;
  }
  else if(direction == DOWN) {
    selected_mode -= 1;

    if(selected_mode < 0)
      selected_mode = NUMBER_OF_MODES-1;
    update_display = true;
  }
  else if(direction == STATIC && button_pressed) {
    current_mode = selected_mode;
    update_display = true;
  }

  if(update_display) {
    displayMode();
  }
}

void displayMode() {

  clearDisplay(WHITE);
  
  for(int i=0; i< NUMBER_OF_MODES; i++) {
    String title = modes[i];

    if(i == MODE_LOOP) {
      title += (String) " [" + (loop_playback ? "Y": "N" ) + "]";
    }

    if(selected_mode == i) {
      setStr(title.c_str(), 0, (i*8)+1, WHITE);
    } else {
      setStr(title.c_str(), 0, (i*8)+1, BLACK);
    }
  }

  updateDisplay();
}

// -------------------------------------------------------------
//  SOUNDBOARD MODE
// -------------------------------------------------------------

void handleSoundBoardMode(int direction, bool button_pressed) {

  if(direction == UP) {
    current_track += 1;
    if(current_track >= NUMBER_OF_TRACKS)
      current_track = 0;
    update_display = true;
  }
  else if(direction == DOWN) {
    current_track -= 1;

    if(current_track < 0)
      current_track = NUMBER_OF_TRACKS -1;
    update_display = true;
  }
  else if(direction == STATIC && button_pressed) {
    playTrack();
  }

  if (update_display) {
    displayTrack();
    update_display = false;
  }
}

bool isPlaying() {
  return digitalRead(BUSY_PIN) == LOW; 
}

void playTrack() {
  if(isPlaying() && playing_track == current_track) {
      myDFPlayer.stop();
  }
  else {
    playing_track = current_track;
    myDFPlayer.playMp3Folder(current_track + 1);
  
    if(loop_playback) {
      int num = myDFPlayer.readCurrentFileNumber();
      myDFPlayer.loop(num);
    }
  } 
}

void displayTrack() {

  clearDisplay(WHITE);

  // Show tracks
  int from = max(min(current_track - 2, NUMBER_OF_TRACKS - 6), 0);
  int to = min(from + 6, NUMBER_OF_TRACKS);
  int pos = 0;
  
  for(int i=from; i< to; i++) {
    if(current_track == i) {
      setStr(sounds[i], 3, (pos*8)+1, WHITE);
    } else {
      setStr(sounds[i], 3, (pos*8)+1, BLACK);
    }
    pos++;
  }

  int scroll_height = 1;
  float inc_per_track = (float)LCD_HEIGHT/(NUMBER_OF_TRACKS*scroll_height);
  int pos_scroll = inc_per_track * (float) current_track;

  for(int i=0; i<3;i++) {
    setPixel(0, pos_scroll+i, true);
    setPixel(1, pos_scroll+i, true);
  }
 
  updateDisplay();
}

// -------------------------------------------------------------
//  VOLUME MODE
// -------------------------------------------------------------

void handleVolumeMode(int direction, bool button_pressed) {
  
  if(direction == UP) {
    current_volume += 1;
    
    if(current_volume > MAX_VOLUME)
      current_volume = MAX_VOLUME;

    update_display = true;
    myDFPlayer.volume(current_volume);
  }
  else if(direction == DOWN) {
    current_volume -= 1;
    
    if(current_volume < 1)
      current_volume = 1;

    update_display = true;
    myDFPlayer.volume(current_volume);
  } 
  else if(direction == STATIC && button_pressed) {
    current_mode = MODE_SELECTION;
  }

  if(update_display) {
    displayVolume();
    update_display = false;
  }
}

void displayVolume() {
  String msg = (String)"Vol:" + current_volume;
  
  clearDisplay(WHITE);
  setStr(msg.c_str(), 0, 0, BLACK);
  updateDisplay();
}

// -------------------------------------------------------------

void doEncoder()
{
  if (digitalRead(encoder0PinA) == digitalRead(encoder0PinB))
  {
    encoder0Pos++;
  }
  else
  {
    encoder0Pos--;
  }

  valRotary = encoder0Pos/2.5;
}


