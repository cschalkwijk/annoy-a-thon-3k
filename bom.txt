This is what I used, not necessarily the best options/choices, but it works :)

Sellers:
www.tinytronics.nl
www.digikey.com
www.sparkfun.com
www.adafruit.com

[ Display ]
Nokia 5110 display
Please note that pin-out might differ per supplier!

https://www.digikey.com/product-detail/en/sparkfun-electronics/LCD-10168/1568-1349-ND/5764484

http://www.aliexpress.com/item/1pcs-White-Backlight-84-48-84x84-LCD-Display-Module-Adapter-PCB-for-Nokia-5110-for-Arduino/32267197519.html

[ Rotary encoder ]
https://www.digikey.com/product-detail/en/tt-electronics-bi/EN11-HSM1BF20/987-1398-ND/2620667

http://www.aliexpress.com/item/5PCS-Plum-handle-20mm-rotary-encoder-coding-switch-EC11-digital-potentiometer-with-switch-5-Pin/32864246557.html

[ Controller ]
Arduino Pro Mini

https://www.digikey.com/product-detail/en/sparkfun-electronics/DEV-11113/1568-1055-ND/5140820

https://www.aliexpress.com/item/Free-Shipping-1pcs-lot-ATMEGA328P-Pro-Mini-328-Mini-ATMEGA328-5V-16MHz-for-Arduino/32340811597.html

[ MP3 Player ]
Will need a Micro SD card (2 Gb is enough for a soundboard)
DF Player mini

https://www.digikey.com/products/en?keywords=dfplayer%20mini

http://www.aliexpress.com/item/1PCS-Mini-MP3-Player-Module-with-Simplified-Output-Speaker-MP3-TF-16P-for-Arduino-UNO/32648794460.html

[ Speaker ]
8 ohm, 2 watt speaker
...

[ Resistors ]
4x 10K Ohm Resistors
1x 1k Ohm Resistor
1x 330 Ohm Resistor
http://www.aliexpress.com/item/100pcs-1-4W-Metal-film-resistor-1R-1M-100R-220R-330R-1K-1-5K-2-2K/32847011772.html

Choos between using either a breadboard & DuPont cables (no soldering required, reusable components) or a prototype board and 'regular' wire. (soldering required, permanent use of components)

[ Breadboard ]
https://www.digikey.com/product-detail/en/global-specialties/GS-830/BKGS-830-ND/5231309

http://www.aliexpress.com/item/Free-Shipping-MB-102-MB102-Breadboard-830Point-Solderless-PCB-Bread-Board-Test-Develop-DIY-for-Bus/32656552625.html

[ DuPont cables ]
male-to-male (!)

https://www.digikey.com/product-detail/en/adafruit-industries-llc/758/1528-1154-ND/5353614

http://www.aliexpress.com/item/Free-shipping-Dupont-line-40pcs-10cm-male-to-male-jumper-wire-Dupont-cable-breadboard-cable-jump/32700513558.html

[ Prototypeboard ]
https://www.aliexpress.com/item/4-pcs-5x7-4x6-3x7-2x8-cm-Double-Side-Board-Copper-prototype-pcb-Universal-Circuit-Board/32795966173.html

[ Wire ]
https://www.aliexpress.com/item/130PC-24AWG-Breadboard-Jumper-Cable-Wires-Kit-Tinning-Double-Tinned-Component-Pack-Colorful-13-Types-10/32810506086.html
or
https://www.aliexpress.com/item/250-m-lots-AWG30-Wrapping-Wire-8-Colors-Single-Strand-Copper-Cable-Ok-Wire-Electrical-Wire/32867495865.html

[ USB to TTL Programmer ]
To program the Arduino with.
Does not end up in the final product.

https://www.digikey.com/product-detail/en/seeed-technology-co-ltd/317990026/1597-1532-ND/5487702

http://www.aliexpress.com/item/1pcs-CP2102-module-USB-to-TTL-serial-UART-STC-download-cable-PL2303-Super-Brush-line-upgrade/32694152202.html

